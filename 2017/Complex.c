#include <stdio.h>
#include <stdlib.h>

typedef struct {
    int re;
    int im;
} complex_t;

complex_t *cadd(complex_t *z, complex_t *w)
{
    complex_t *c = malloc(sizeof(complex_t));
    c->re = z->re + w->re;
    c->im = z->im + w->im;

    return c;
}

complex_t *csub(complex_t *z, complex_t *w)
{
    complex_t *c = malloc(sizeof(complex_t));
    c->re = z->re - w->re;
    c->im = z->im - w->im;

    return c;
}

complex_t *cmult(complex_t *z, complex_t *w)
{
    complex_t *c = malloc(sizeof(complex_t));

    int x = z->re;
    int y = z->im;
    int u = w->re;
    int v = w->im;

    c->re = (x*u) - (y*v);
    c->im = (x*v) + (y*u);

    return c;
}

complex_t *cdiv(complex_t *z, complex_t *w)
{
    complex_t *c = malloc(sizeof(complex_t));
    complex_t *reciprocal = malloc(sizeof(complex_t));

    int a = z->re;
    int b = z->im;
    
    int real = a / (a*a + b*b);
    int img = b / (a*a + b*b);

    reciprocal->re = real;
    reciprocal->im = img;

    c = cmult(reciprocal, w);
    free(reciprocal);

    return c;
}

void zstr(complex_t *z)
{
    printf("(%i + %ii)\n", z->re, z->im);
}

int main(int argc, char const *argv[])
{
    int x = atoi(argv[1]);
    int y = atoi(argv[2]);
    int u = atoi(argv[3]);
    int v = atoi(argv[4]);

    complex_t *z = malloc(sizeof(complex_t));
    complex_t *w = malloc(sizeof(complex_t));

    z->re = x;
    z->im = y;
    w->re = u;
    w->im = v;

    zstr(z);
    zstr(w);

    free(z);
    free(w);

    return 0;
}

