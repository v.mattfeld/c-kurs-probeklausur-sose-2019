#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>

typedef struct {
	char name[32];
	int number;
	char checksum;
} data_t;

data_t *new_data(char *name, int number);

data_t *new_data(char *name, int number)
{
	data_t *d = (data_t*) malloc(sizeof(data_t));
	strcpy(d->name, name);
	d->number = number;

	// calculate checksum
	char *c;
	for(c = name; *c != '\0'; c++){
		number += (int) *c;
	}
	d->checksum = number % CHAR_MAX;

	return d;
}

int main(int argc, char const *argv[])
{
	data_t *data = new_data("Valerius", 10);
	printf("name: %s\n", data->name);
	printf("number: %d\n", data->number);
	printf("checksum: %c\n", data->checksum);

	free(data);
	return 0;
}
