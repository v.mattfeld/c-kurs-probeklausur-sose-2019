#include "a03-testing.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char const *argv[])
{
	int *start = calloc(argc, sizeof(int));
	int *end = start+argc;
	int reverse = 0;

	int idx = 0;
	
	for (int i = 0; i < argc; ++i)
	{
		int k = atoi(argv[i]);
		if(k)
		{
			start[idx] = k;
			idx++;
		}
	}
	printf("first element %i\n", *start);
	printf("last element %i\n", *(end-2));

	// end = (start+argc);
	print_arr(start, idx);

	int swaps = recSort(start, end-2, reverse);
	
	print_arr(start, idx); // fml

	free(start);

	printf("Swaps: %i\n", swaps);

	return 0;
}
