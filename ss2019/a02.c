#include "stdio.h"
#include "stdlib.h"

int sequence(int x, int n);

int sequence(int x, int n) 
{
	if (n == 0) return x;
	if (n < 0) return 0;

	int *results = calloc(n, sizeof(int));

	for (int i = 0; i <= n; ++i)
	{
		if(i == 0)
		{
			results[i] = x;
		} else if (results[i-1] % 2 == 0)
		{
			results[i] = results[i-1] / 2;
		} else
		{
			results[i] = 3 * results[i-1] + 1;
		}
	}

	int ret = results[n];
	free(results); 

	return ret;
}
