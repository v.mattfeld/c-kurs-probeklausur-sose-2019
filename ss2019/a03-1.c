#include "a03-testing.h"
#include <stdio.h>

// Algoerklaerung der Aufgabe ist stark ausbaufaehig...

void swap(int *a, int *b) 
{
	int tmp = *a;
	*a = *b;
	*b = tmp;
}

// returns no. of swaps made
int recSort(int *start, int *end, int reverse)
{
	printf("%s\n", reverse ? "reverse" : "forward");
	// handles 0 or 1 length arrays
	if (start == end || start == (end-1)) return 0;
	if (start >= end) return 0;

	int *p;
	int swaps = 0;

	if (reverse)
	  p = end-1;
	else
	  p = start+1;
	
	while(p < end && p > start)
	{
		if(*(p-1) < *p)
		{
			swap((p-1), p);
			swaps++;
		}
		if (reverse)
		  p--;
		else
		  p++;
	}

	if (swaps == 0) return 0;

	if (reverse)
		reverse = 0;
	else
		reverse = 1;

	if (reverse)
	  swaps += recSort(start+1, end, 0);
	else
	  swaps += recSort(start, end-1, 1);

	return swaps;
}

// Helper function; prints array
void print_arr(int *arr, int s)
{
	printf("{ ");
	for(int i = 0; i < s; i++)
	{
		printf("%d ", arr[i]);
	}
	printf("}\n");
}

// int main()
// {
//   int x[] = {1,2,7,4,5,6};
//   int size = 6;

//   int res =recSort(x, (x+size), 0);
//   printf("Swaps: %i\n", res); // shows how many swaps were made
//   print_arr(x, size);
//   return 0;
// }
