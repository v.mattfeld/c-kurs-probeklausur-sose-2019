#include "stdio.h"
#include "stdlib.h"
#include "string.h"

// typedef <datentypdefinition> <name>
typedef struct
{
	int len;
	int *arr; // ist literally ein pointer zu "arr"
} vecc;

vecc *new(int size, int *array) 
{
	vecc *v = malloc(sizeof(vecc));
	v->arr = calloc(size, sizeof(int));
	memcpy(v->arr, array, size * sizeof(int));
	v->len = size;
	return v;
}

vecc *(*Vecc)(int, int *) = &new;

void *add(int n, vecc *v)
{
	int size = v->len;
	void *res = realloc(v->arr, (1 + size) * sizeof(int)); 
	v->arr[size] = n;
	v->len = ++size;
	return res;
}

void free_vecc(vecc *v)
{
	free(v->arr);
	free(v);
}

int main()
{
	//array *m = malloc(sizeof(array)); // den shit auf den heap packen yeah
	// (*m).arr == m->arr
	int x[] = {22, 33, 52, 23, 51};
	vecc *v = Vecc(5, x);

	printf("%d\n", *(v->arr + 4));
	add(0, v);
	printf("%d\n", *(v->arr + 5));
	printf("%i\n", v->len);
	char *str = "Hallo, Huso";

	printf("%ld\n", strlen(str));

	free_vecc(v);

	return 0;
}
